Shader "Custom/ObjectMoveShader" {
	Properties{
		_MainTex("Texture", 2D) = "white" {}
		_Displacement("Displacement", Range(-1, 1)) = 0
	}
		SubShader{
			Tags {"Queue" = "Transparent" "RenderType" = "Opaque"}
			LOD 100

			Pass {
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"

				struct appdata {
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f {
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};

				sampler2D _MainTex;
				float _Displacement;

				v2f vert(appdata v) {
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = v.uv;
					return o;
				}

				fixed4 frag(v2f i) : SV_Target {
					float2 uv = i.uv;
					uv.x += _Displacement;
					fixed4 col = tex2D(_MainTex, uv);
					return col;
				}
				ENDCG
			}
		}
}

