﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    public float flyForce = 20; // Fuerza de la propulsión para volar
    private float initialSize;
    private int i = 0;
    private bool floored;
    private bool flying; // Variable booleana para controlar si el jugador está volando o no


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
    }

    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        Jump();
        Duck();
        Volar(); // Nueva función para controlar el vuelo
    }

    private void Jump()
    {
        if (floored && !flying) // No se puede saltar mientras se está volando
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void Duck()
    {
        if (floored && !flying) // No se puede agachar mientras se está volando
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void Volar()
    {
        if (Input.GetKey(KeyCode.Space) && !floored) // Para volar, se tiene que presionar la barra espaciadora y estar en el aire
        {
            flying = true; // Se establece la variable de volar en true
            rb.AddForce(new Vector3(0, flyForce, 0), ForceMode.Impulse); // Se aplica la fuerza de la propulsión
        }
        else
        {
            flying = false; // Si no se está presionando la barra espaciadora, se establece la variable de volar en false
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
        }


        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }

}