using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Nafta : MonoBehaviour
{
    public static float naftavelocity;
    private Rigidbody rb;
    public float extraTime = 5f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        rb.AddForce(new Vector3(-naftavelocity, 0, 0), ForceMode.Force);
        OutOfBounds();
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            BarraProgreso BarraProgreso = other.GetComponent<BarraProgreso>();

            if (BarraProgreso != null)
            {
                BarraProgreso.AddTime(extraTime);
            }

            Destroy(gameObject);

        }
    }
}

