using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float cantidadNafta = 0f; // La cantidad de nafta que tiene el jugador

    // Este m�todo se llama cuando el jugador recibe nafta de la nafta
    void RecibirNafta(float cantidad)
    {
        cantidadNafta += cantidad; // Aumenta la cantidad de nafta del jugador
    }

    // Este m�todo se llama cada frame
    void Update()
    {
        cantidadNafta -= Time.deltaTime * 10f; // La cantidad de nafta disminuye cada segundo
        if (cantidadNafta <= 0f)
        {
            // Si la cantidad de nafta es menor o igual a cero, el jugador pierde
            Debug.Log("Perdiste");
            // Aqu� puedes agregar cualquier otra acci�n que quieras que ocurra cuando el jugador pierde
        }
    }
}
