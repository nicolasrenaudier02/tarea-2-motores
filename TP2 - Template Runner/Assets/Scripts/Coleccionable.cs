using UnityEngine;
using UnityEngine.UI;

public class Coleccionable : MonoBehaviour
{
    public Slider slider; // Variable p�blica que contendr� una referencia al componente Slider
    public float sliderValueToAdd = 0.1f; // Valor a agregar al slider cuando se recoge el objeto

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            slider.value += sliderValueToAdd; // Aumenta el valor del slider
            Destroy(gameObject); // Destruye el objeto coleccionable
        }
    }
}
